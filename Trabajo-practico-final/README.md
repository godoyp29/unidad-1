Integrantes: Pedro Godoy

Consignas implementadas:
Las consignas implementadas fueron todas las obligatorias, siendo estas:
-Dar de alta y listar estudiantes
-Buscar estudiantes por nombre
-Buscar estudiantes por rango de edad
-Dar de alta y listar materias
-Anotarse en una materia
-Rendir una materia

Opcionales:
-Cálculo de promedios
-Generar datos de forma masiva
-Poder elegir materia de un listado reducido usando un archivo de configuracion
-Generar estudiantes de prueba y materias aleatorias de forma masiva
-Estructura para simular correlatividad en base a un archivo de configuracion

ACLARACION IMPORTANTE:
En el archivo lector_csv se utiliza el metodo strtok que ademas de ser complicado de usar, puede ser que no sea portable a otros sistemas operativos. Por la informacion que tengo, se podria cambiar strtok por strtok_s pero no estoy seguro.
Quedo a su disposicion para cualquier consulta de funcionamiento de este metodo y si necesita que le mande pruebas de como funciona si es que no lo puede hacer correr por el sistema operativo.

Link al video explicacion: https://www.youtube.com/watch?v=ckZRIzj2Fcc
