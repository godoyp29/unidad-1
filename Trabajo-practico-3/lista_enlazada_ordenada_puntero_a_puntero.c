#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

//Implementar una lista de enteros ordenada.
//Cada elemento que agrego queda ordenado en la lista, de manera que al imprimirla se imprime automáticamente ordenada.

typedef struct estructuraNodo
{
	int valor;
	struct estructuraNodo *proximo;
} Nodo;

int comparador(Nodo *nodo1, Nodo *nodo2)
{
	bool menorAMayor = true;
	Nodo *nodoNuevo1;
	Nodo *nodoNuevo2;
	if (menorAMayor)
	{
		nodoNuevo1 = nodo1;
		nodoNuevo2 = nodo2;
	}
	else
	{
		nodoNuevo2 = nodo1;
		nodoNuevo1 = nodo2;
	}

	return (nodoNuevo1->valor <= nodoNuevo2->valor) ? 1 : 0;
}

void agregarElemento(Nodo **lista, int valor)
{
	Nodo *nodoNuevo = malloc(sizeof(Nodo));
	nodoNuevo->valor = valor;
	nodoNuevo->proximo = NULL;

	if (*lista == NULL)
	{
		*lista = nodoNuevo;
	}
	else
	{
		if (comparador(nodoNuevo, *lista))
		{
			Nodo *cabeza;
			cabeza = nodoNuevo;
			cabeza->proximo = *lista;
			*lista = cabeza;
		}
		else
		{
			Nodo *cursor = *lista;
			while ((cursor->proximo != NULL) && comparador(cursor->proximo, nodoNuevo))
			{
				cursor = cursor->proximo;
			}
			if (cursor->proximo == NULL)
			{
				cursor->proximo = nodoNuevo;
			}
			else
			{
				nodoNuevo->proximo = cursor->proximo;
				cursor->proximo = nodoNuevo;
			}
		}
	}
}

Nodo *crearLista()
{
	Nodo *lista = malloc(sizeof(Nodo));
	lista = NULL;
	return lista;
}

bool estaVacia(Nodo *lista)
{
	return lista == NULL;
}

int obtenerLargo(Nodo *lista)
{
	int nodos;

	if (estaVacia(lista))
	{
		nodos = 0;
	}
	else
	{
		nodos = 1;
		Nodo *cursor = lista;
		while (cursor->proximo != NULL)
		{
			cursor = cursor->proximo;
			nodos += 1;
		}
	}
	return nodos;
}

void imprimirLista(Nodo *lista)
{

	if (estaVacia(lista))
	{
		printf("La lista esta vacia");
	}
	else
	{
		int largo = obtenerLargo(lista);
		Nodo *cursor = lista;

		for (int i = 0; i < largo; i++)
		{
			printf("[%d]->", cursor->valor);
			cursor = cursor->proximo;
		}
	}
}

int obtenerElemento(Nodo *lista, int posicion)
{
	if ((posicion < 0) || (posicion > obtenerLargo(lista) - 1))
	{
		assert(0);
	}
	else
	{
		Nodo *cursor = lista;
		for (int i = 0; i < posicion; i++)
		{
			cursor = cursor->proximo;
		}
		return cursor->valor;
	}
}

void borrarElemento(Nodo **lista, int posicion)
{

	int largoLista = obtenerLargo(*lista) - 1;

	if ((posicion < 0) || (posicion > largoLista))
	{
		assert(0);
	}
	else
	{
		if (posicion == 0)
		{
			Nodo *nodoTemporal = malloc(sizeof(Nodo));
			nodoTemporal->proximo = *lista;
			*lista = (*lista)->proximo;
			free(nodoTemporal);
			nodoTemporal = NULL;
		}
		else if (posicion == largoLista)
		{
			Nodo *cursor = *lista;
			for (int i = 0; i < posicion - 1; i++)
			{
				cursor = cursor->proximo;
			}
			free(cursor->proximo);
			cursor->proximo = NULL;
		}
		else
		{
			Nodo *nodoTemporal = malloc(sizeof(Nodo));
			Nodo *cursor = *lista;
			for (int i = 0; i < posicion - 1; i++)
			{
				cursor = cursor->proximo;
			}
			nodoTemporal = cursor->proximo;
			cursor->proximo = cursor->proximo->proximo;
			nodoTemporal->proximo = NULL;
			free(nodoTemporal);
			nodoTemporal = NULL;
		}
	}
}

int main()
{
	Nodo *lista = crearLista();
	agregarElemento(&lista, 4);
	agregarElemento(&lista, 2);
	agregarElemento(&lista, 3);
	agregarElemento(&lista, 5);
	agregarElemento(&lista, 1);
	agregarElemento(&lista, 4);
	agregarElemento(&lista, 5);
	agregarElemento(&lista, 0);
	agregarElemento(&lista, -1);

	imprimirLista(lista);
	return 0;
}
