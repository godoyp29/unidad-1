#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

//Implementar una lista de enteros ordenada.
//Cada elemento que agrego queda ordenado en la lista, de manera que al imprimirla se imprime automáticamente ordenada.

typedef struct estructuraNodo
{
	int valor;
	struct estructuraNodo *proximo;
} Nodo;

typedef struct estructuraListaEnlazada
{
	bool orden;
	struct estructuraNodo *cabeza;
} ListaEnlazada;

int comparador(Nodo *nodo1, Nodo *nodo2, bool orden)
{
	bool menorAMayor = orden;
	Nodo *nodoNuevo1;
	Nodo *nodoNuevo2;
	if (menorAMayor)
	{
		nodoNuevo1 = nodo1;
		nodoNuevo2 = nodo2;
	}
	else
	{
		nodoNuevo2 = nodo1;
		nodoNuevo1 = nodo2;
	}

	return (nodoNuevo1->valor <= nodoNuevo2->valor) ? 1 : 0;
}

void agregarElemento(ListaEnlazada *lista, int valor)
{
	Nodo *nodoNuevo = malloc(sizeof(Nodo));
	nodoNuevo->valor = valor;
	nodoNuevo->proximo = NULL;

	if (lista->cabeza == NULL)
	{
		lista->cabeza = nodoNuevo;
	}
	else
	{
		if (comparador(nodoNuevo, lista->cabeza, lista->orden))
		{
			// Nodo *nodoTemporal;
			nodoNuevo->proximo = lista->cabeza;
			lista->cabeza = nodoNuevo;
			// nodoTemporal = nodoNuevo;
			// lista->cabeza->proximo = lista->cabeza;
			// lista->cabeza = nodoTemporal;
		}
		else
		{
			Nodo *cursor = lista->cabeza;
			while ((cursor->proximo != NULL) && comparador(cursor->proximo, nodoNuevo, lista->orden))
			{
				cursor = cursor->proximo;
			}
			if (cursor->proximo == NULL)
			{
				cursor->proximo = nodoNuevo;
			}
			else
			{
				nodoNuevo->proximo = cursor->proximo;
				cursor->proximo = nodoNuevo;
			}
		}
	}
}

ListaEnlazada *crearLista(bool orden)
{
	ListaEnlazada *lista = malloc(sizeof(ListaEnlazada));
	lista->orden = orden;
	lista->cabeza = NULL;
	return lista;
}

bool estaVacia(ListaEnlazada *lista)
{
	return lista->cabeza == NULL;
}

int obtenerLargo(ListaEnlazada *lista)
{
	int nodos;

	if (estaVacia(lista))
	{
		nodos = 0;
	}
	else
	{
		nodos = 1;
		Nodo *cursor = lista->cabeza;
		while (cursor->proximo != NULL)
		{
			cursor = cursor->proximo;
			nodos += 1;
		}
	}
	return nodos;
}

void imprimirLista(ListaEnlazada *lista)
{

	if (estaVacia(lista))
	{
		printf("La lista esta vacia");
	}
	else
	{
		int largo = obtenerLargo(lista);
		Nodo *cursor = lista->cabeza;

		for (int i = 0; i < largo; i++)
		{
			printf("[%d]->", cursor->valor);
			cursor = cursor->proximo;
		}
	}
}

int obtenerElemento(ListaEnlazada *lista, int posicion)
{
	if ((posicion < 0) || (posicion > obtenerLargo(lista) - 1))
	{
		assert(0);
	}
	else
	{
		Nodo *cursor = lista->cabeza;
		for (int i = 0; i < posicion; i++)
		{
			cursor = cursor->proximo;
		}
		return cursor->valor;
	}
}

void borrarElemento(ListaEnlazada *lista, int posicion)
{

	int largoLista = obtenerLargo(lista) - 1;

	if ((posicion < 0) || (posicion > largoLista))
	{
		assert(0);
	}
	else
	{
		if (posicion == 0)
		{
			Nodo *nodoTemporal = malloc(sizeof(Nodo));
			nodoTemporal->proximo = lista->cabeza;
			lista->cabeza = lista->cabeza->proximo;
			free(nodoTemporal);
			nodoTemporal = NULL;
		}
		else if (posicion == largoLista)
		{
			Nodo *cursor = lista->cabeza;
			for (int i = 0; i < posicion - 1; i++)
			{
				cursor = cursor->proximo;
			}
			free(cursor->proximo);
			cursor->proximo = NULL;
		}
		else
		{
			Nodo *nodoTemporal = malloc(sizeof(Nodo));
			Nodo *cursor = lista->cabeza;
			for (int i = 0; i < posicion - 1; i++)
			{
				cursor = cursor->proximo;
			}
			nodoTemporal = cursor->proximo;
			cursor->proximo = cursor->proximo->proximo;
			nodoTemporal->proximo = NULL;
			free(nodoTemporal);
			nodoTemporal = NULL;
		}
	}
}

int main()
{
	ListaEnlazada *lista = crearLista(true);

	agregarElemento(lista, 4);
	agregarElemento(lista, 2);
	agregarElemento(lista, 3);
	agregarElemento(lista, 5);
	agregarElemento(lista, 1);
	agregarElemento(lista, 4);
	agregarElemento(lista, 5);
	agregarElemento(lista, 0);
	agregarElemento(lista, -1);
	borrarElemento(lista, 5);
	agregarElemento(lista, -2);
	agregarElemento(lista, 4);
	borrarElemento(lista, 6);
	borrarElemento(lista, 7);

	imprimirLista(lista);
	return 0;
}
