Separe los archivos de las distintas implentaciones con orden, sin orden, con puntero y con puntero a puntero para que se pueda corregir mejor por separado en vez de tener todos los commits juntos.
Ademas de eso, realice una implementacion diferente de la lista ordenada. Para hacer la lista ordenada, use (como se puede usar en java) una especie de comparador que lo simule con un metodo. De esta forma, tengo la posibilidad de elegir si la lista se ordena de menor a mayor, o al reves, sin tener que cambiar los > y < de las condiciones cada vez que agrego un nodo.
El problema con eso es que hay que cambiar manualmente la variable que maneja el orden, desde dentro del codigo, por esa duda me surgio la idea de hacer esta nueva implementacion.
Cree una struct nueva que modela la lista, y dentro de ella puedo almacenar el orden de la misma, eligiendolo cuando instancio la struct en el main. Tambien dentro de esta struct tengo un puntero a la cabeza para manejar los nodos. Con esta implementacion no me parecio necesario hacerlo con puntero a puntero ya que nunca tengo que reemplazar la cabeza como en la implementacion anterior.

Aca vienen mis dudas que intente deducir por mi parte y buscando informacion por internet con respecto a porque tenemos que devolver la lista usando solo un puntero si en el tp anterior, solo pasabamos la estructura como puntero y desde dentro del metodo, se cambiaban los datos de la misma sin tener que devolver nada.
Por lo que entiendo, al usar solo un puntero en la implementacion de la lista, el unico motivo por el que tenemos que devolver la lista es que existe el caso en el que la lista no este creada y debamos pisar el nodo:

```
if (lista == NULL){
lista = nodoNuevo;
}
```


En este caso, no estariamos modificando los datos de la estructura como en el tp2, sino que estariamos pisando todo.
Ahora, si en vez de crear la lista con un nodo = NULL, lo creasemos con un nodo con valores default, y usaramos ese nodo para comenzar la implementacion, ahi no tendriamos necesidad de devolver nada y podriamos trabajar igual que en el tp2, ya que nunca estamos pisando el puntero de la cabeza, solo cambiando los valores que contiene la struct.
Estas fueron las pruebas que hice para llegar a esa conclusion

Esto seria lo que haria el metodo crearLista()
---

```
Nodo \*nodoNuevo1 = malloc(sizeof(Nodo));
nodoNuevo1->valor = 1; 
nodoNuevo1->proximo = NULL;
```


---

```
agregarElemento(nodoNuevo1, 2);
agregarElemento(nodoNuevo1, 3);
printf("%d\n", nodoNuevo1->valor);
printf("%d\n", nodoNuevo1->proximo->valor);
printf("%d\n", nodoNuevo1->proximo->proximo->valor);
```


Luego de crear el nodo, ya puedo trabajar agregando y borrando nodos.
Le encuentro 2 problemas a esto, el primero es que si borro por completo la lista tengo que decidir entre si quiero que lo que quede guardado en el nodo que elegi como cabeza sea NULL (en ese caso, cuando agrege un nodo nuevo si voy a tener que devolver) o que tenga valores default, que yo pueda reconocer. La segunda es la que me llevo a hacer la implementacion propia, es que nose que valores se puede elegir como default ya que todos son validos como datos.

Entiendo que la implementacion con puntero a puntero soluciona este problema de tener que devolver el nodo de la cabeza si es que es NULL y lo quiero pisar con uno nuevo.

Mi pregunta basicamente es si mi conclusion con respecto a porque devolvermos la lista, y al puntero a puntero, es correcta.

Ademas por lo que encontre, c no tiene excepciones incorporadas, por eso use el assert(0) pero no se si es buena practica, o que deberia usar en su defecto.
