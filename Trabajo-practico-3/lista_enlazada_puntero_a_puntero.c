#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

// -Crear e inicializar la lista -
// -Agregar un elemento -
// -Obtener el largo de la lista -
// -Obtener un elemento N de la lista -
// -Eliminar un elemento N de la lista -
// -Imprimir la lista

typedef struct estructuraNodo
{
	int valor;
	struct estructuraNodo *proximo;
} Nodo;

void agregarElemento(Nodo **lista, int valor)
{
	Nodo *nodoNuevo = malloc(sizeof(Nodo));
	nodoNuevo->valor = valor;
	nodoNuevo->proximo = NULL;

	if (*lista == NULL)
	{
		*lista = nodoNuevo;
	}
	else
	{
		Nodo *cursor = *lista;
		while (cursor->proximo != NULL)
		{
			cursor = cursor->proximo;
		}
		cursor->proximo = nodoNuevo;
	}
}

Nodo *crearLista()
{
	Nodo *lista = malloc(sizeof(Nodo));
	lista = NULL;
	return lista;
}

bool estaVacia(Nodo *lista)
{
	return lista == NULL;
}

int obtenerLargo(Nodo *lista)
{
	int nodos;

	if (estaVacia(lista))
	{
		nodos = 0;
	}
	else
	{
		nodos = 1;
		Nodo *cursor = lista;
		while (cursor->proximo != NULL)
		{
			cursor = cursor->proximo;
			nodos += 1;
		}
	}
	return nodos;
}

void imprimirLista(Nodo *lista)
{
	if (estaVacia(lista))
	{
		printf("La lista esta vacia\n");
	}
	else
	{
		int largo = obtenerLargo(lista);
		Nodo *cursor = lista;
		for (int i = 0; i < largo; i++)
		{
			printf("[%d]->", cursor->valor);
			cursor = cursor->proximo;
		}
		printf("\n");
	}
}

int obtenerElemento(Nodo *lista, int posicion)
{
	if ((posicion < 0) || (posicion > obtenerLargo(lista) - 1))
	{
		assert(0);
	}
	else
	{
		Nodo *cursor = lista;
		for (int i = 0; i < posicion; i++)
		{
			cursor = cursor->proximo;
		}
		return cursor->valor;
	}
}

void borrarElemento(Nodo **lista, int posicion)
{
	int largoLista = obtenerLargo(*lista) - 1;

	if ((posicion < 0) || (posicion > largoLista))
	{
		assert(0);
	}
	else
	{
		if (posicion == 0)
		{
			Nodo *nodoTemporal = malloc(sizeof(Nodo));
			nodoTemporal->proximo = *lista;
			*lista = (*lista)->proximo;
			free(nodoTemporal);
			nodoTemporal = NULL;
		}
		else if (posicion == largoLista)
		{
			Nodo *cursor = *lista;
			for (int i = 0; i < posicion - 1; i++)
			{
				cursor = cursor->proximo;
			}
			free(cursor->proximo);
			cursor->proximo = NULL;
		}
		else
		{
			Nodo *nodoTemporal = malloc(sizeof(Nodo));
			Nodo *cursor = *lista;
			for (int i = 0; i < posicion - 1; i++)
			{
				cursor = cursor->proximo;
			}
			nodoTemporal = cursor->proximo;
			cursor->proximo = cursor->proximo->proximo;
			nodoTemporal->proximo = NULL;
			free(nodoTemporal);
			nodoTemporal = NULL;
		}
	}
}

int main()
{
	Nodo *lista = crearLista();
	agregarElemento(&lista, 2);
	agregarElemento(&lista, 3);
	agregarElemento(&lista, 4);

	imprimirLista(lista);

	borrarElemento(&lista, 1);

	imprimirLista(lista);

	agregarElemento(&lista, 5);
	agregarElemento(&lista, 7);

	imprimirLista(lista);

	borrarElemento(&lista, 0);

	imprimirLista(lista);

	agregarElemento(&lista, 6);

	imprimirLista(lista);
	printf("%s %d \n", "El largo de la lista es: ", obtenerLargo(lista));
	imprimirLista(lista);
	printf("El elemento en la posicion %d es: %d", 2, obtenerElemento(lista, 2));
	return 0;
}
