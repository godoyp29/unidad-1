#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	char *marca;
	char *tipo;
	int precio;
} Teclado;

void setMarca(Teclado *teclado, char *marca)
{

	teclado->marca = marca;
}

void setTipo(Teclado *teclado, char *tipo)
{

	teclado->tipo = tipo;
}

void setPrecio(Teclado *teclado, int precio)
{

	teclado->precio = precio;
}

int main(int argc, char const *argv[])
{

	Teclado *teclado = malloc(sizeof(Teclado));
	setMarca(teclado, "RedDragon");
	setTipo(teclado, "Mecanico");
	setPrecio(teclado, 6000);

	printf("Marca: %s, Tipo: %s, Precio: %d\n", teclado->marca, teclado->tipo, teclado->precio);

	return 0;
}
