#include <stdio.h>

typedef struct
{
	char *marca;
	char *tipo;
	int precio;
} Teclado;

int main(int argc, char const *argv[])
{
	Teclado teclado;
	teclado.marca = "Redragon";
	teclado.tipo = "Mecanico";
	teclado.precio = 6000;

	printf("Marca: %s, Tipo: %s, Precio: %d\n", teclado.marca, teclado.tipo, teclado.precio);

	return 0;
}
