#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	char *nombre;
	char *apellido;
	int dni;
} Hijo;

typedef struct
{
	char *nombre;
	char *apellido;
	int dni;
	Hijo *primerHijo;
	Hijo *segundoHijo;
} Persona;

void setNombreYApellidoPersona(Persona *persona, char *nombre, char *apellido)
{
	persona->nombre = nombre;
	persona->apellido = apellido;
}

void setDniPersona(Persona *persona, int dni)
{
	persona->dni = dni;
}

void setNombreYApellidoHijo(Hijo *hijo, char *nombre, char *apellido)
{
	hijo->nombre = nombre;
	hijo->apellido = apellido;
}

void setDniHijo(Hijo *hijo, int dni)
{
	hijo->dni = dni;
}

int main(int argc, char const *argv[])
{
	Persona *persona = malloc(sizeof(Persona));
	persona->primerHijo = malloc(sizeof(Hijo));
	persona->segundoHijo = malloc(sizeof(Hijo));

	setNombreYApellidoPersona(persona, "Pedro", "Godoy");
	setDniPersona(persona, 41824376);
	setNombreYApellidoHijo(persona->primerHijo, "Juan", "Godoy");
	setDniHijo(persona->primerHijo, 78824376);
	setNombreYApellidoHijo(persona->segundoHijo, "Marcos", "Godoy");
	setDniHijo(persona->segundoHijo, 79824376);

	printf("Nombre: %s, Apellido: %s, DNI: %d\n", persona->nombre, persona->apellido, persona->dni);
	printf("Nombre: %s, Apellido: %s, DNI: %d\n", persona->primerHijo->nombre, persona->primerHijo->apellido, persona->primerHijo->dni);
	printf("Nombre: %s, Apellido: %s, DNI: %d\n", persona->segundoHijo->nombre, persona->segundoHijo->apellido, persona->segundoHijo->dni);

	return 0;
}
