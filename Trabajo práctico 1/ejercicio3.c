#include <stdio.h>

/*
	A partir de un listado de números, determinar el mínimo.
*/

int determinar_minimo(int numeros[], int longitud)
{
	int minimo = numeros[0];
	for (int i = 1; i < longitud; i++)
	{
		if (numeros[i] < minimo)
		{
			minimo = numeros[i];
		}
	}
	return minimo;
}

int main(int argc, char const *argv[])
{
	int numeros[] = {0, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	printf("%d", determinar_minimo(numeros, sizeof(numeros) / sizeof(numeros[0])));
}
