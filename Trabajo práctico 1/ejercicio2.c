#include <stdio.h>
/*
	A partir de un listado de números, determinar el máximo.
*/
int determinar_maximo(int numeros[], int longitud)
{
	int maximo = numeros[0];
	for (int i = 1; i < longitud; i++)
	{
		if (numeros[i] > maximo)
		{
			maximo = numeros[i];
		}
	}
	return maximo;
}

int main(int argc, char const *argv[])
{
	int numeros[] = {10, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	printf("%d", determinar_maximo(numeros, sizeof(numeros) / sizeof(numeros[0])));
}
