#include <stdio.h>
#include <stdbool.h>

/*
	Diseñar un menú navegable donde cada opción muestre una frase
	distinta. Debe retornar al menú a menos que se elija la opción “Salir”.
*/

int main(int argc, char const *argv[])
{

	bool salir = false;
	int eleccion;

	while (!salir)
	{
		printf("\n-Menu-\n");
		printf("1. Opcion 1\n");
		printf("2. Opcion 2\n");
		printf("3. Opcion 3\n");
		printf("4. Salir\n");

		printf("Elija una opcion: ");
		scanf("%d", &eleccion);
		switch (eleccion)
		{
		case 1:
			printf("Eligio la opcion 1");
			break;

		case 2:
			printf("Eligio la opcion 2");
			break;

		case 3:
			printf("Eligio la opcion 3");
			break;

		case 4:
			printf("Saliendo del menu");
			salir = true;
			break;

		default:
			printf("Las opciones son 1,2,3 o 4");
			break;
		}
		printf("\n\n");
	}
	return 0;
}
