#include <stdio.h>

/*
	A partir del ingreso de tres enteros determinar el promedio.
*/

int main(int argc, char const *argv[])
{
	int num1, num2, num3;

	printf("Ingresar tres numeros enteros: \n");
	scanf("%d %d %d", &num1, &num2, &num3);
	printf("El promedio es: %.2f", ((num1 + num2 + num3) / 3.0));

	return 0;
}
