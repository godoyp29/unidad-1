#include <stdio.h>

/*
	Programar una rutina que reciba nombre y edad de una persona y las
	imprima por pantalla.
*/

int main(int argc, char const *argv[])
{
	char nombre[100];
	int edad;

	printf("Ingrese su nombre\n");
	scanf("%[^\n]", nombre); //%[^\n] es para leer strings con espacios, sino, se saltea el proximo scanf
	printf("Ingrese su edad\n");
	scanf("%d", &edad);
	printf("Su nombre es %s y su edad es %d.", nombre, edad);

	return 0;
}
