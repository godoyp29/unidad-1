#include <stdio.h>

/*
	Imprimir todos los caracteres ASCII.
*/

int main(int argc, char const *argv[])
{
	for (int i = 0; i <= 255; i++)
	{
		printf("%c \n", i);
	}
	return 0;
}
