#include <stdio.h>

/*
	Determinar si el número ingresado es par o no.
*/

int main(int argc, char const *argv[])
{
	int numero;

	printf("Ingrese un numero: ");
	scanf("%d", &numero);
	printf("%s", (numero % 2 == 0) ? "Es par" : "Es impar");

	return 0;
}
